// FOR LOCAL UPLOADING
// import path from 'path';
// import express from 'express';
// import multer from 'multer';

// const router = express.Router();

// const storage = multer.diskStorage({
//   destination(req, file, cb) {
//     cb(null, 'uploads/');
//   },
//   filename(req, file, cb) {
//     cb(
//       null,
//       `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`
//     );
//   },
// });

// function fileFilter(req, file, cb) {
//   const filetypes = /jpe?g|png|webp/;
//   const mimetypes = /image\/jpe?g|image\/png|image\/webp/;

//   const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
//   const mimetype = mimetypes.test(file.mimetype);

//   if (extname && mimetype) {
//     cb(null, true);
//   } else {
//     cb('Error: Images only!', false);
//   }
// }

// const upload = multer({ storage, fileFilter });
// const uploadSingleImage = upload.single('image');

// router.post('/', (req, res) => {
//   uploadSingleImage(req, res, (err) => {
//     if (err) {
//       console.log(err);
//       return res.status(400).send({ message: err instanceof multer.MulterError ? err.message : 'An error occurred when uploading.' });
//     }
    
//     if (!req.file) {
//       return res.status(400).send({ message: 'No file uploaded' });
//     }

//     res.status(200).send({
//       message: 'Image uploaded successfully',
//       image: `/${req.file.path}`,
//     });
//   });
// });

// export default router;

//FOR GOOGLE UPLOADING
// import path from 'path';
// import express from 'express';
// import multer from 'multer';
// import { Storage } from '@google-cloud/storage';

// const router = express.Router();

// // Local storage configuration
// const localStorage = multer.diskStorage({
//   destination(req, file, cb) {
//     cb(null, 'uploads/');
//   },
//   filename(req, file, cb) {
//     cb(
//       null,
//       `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`
//     );
//   },
// });

// // Google Cloud Storage configuration
// const storage = new Storage({
//   projectId: 'capable-code-395103',
//   keyFilename: './keys/bucketKey.json',
// });
// const bucketName = 'capstone-3-toya';
// const bucket = storage.bucket(bucketName);
// const cloudStorage = multer.memoryStorage();

// function fileFilter(req, file, cb) {
//   const filetypes = /jpe?g|png|webp/;
//   const mimetypes = /image\/jpe?g|image\/png|image\/webp/;

//   const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
//   const mimetype = mimetypes.test(file.mimetype);

//   if (extname && mimetype) {
//     cb(null, true);
//   } else {
//     cb('Error: Images only!', false);
//   }
// }

// const uploadLocal = multer({ storage: localStorage, fileFilter });
// const uploadCloud = multer({ storage: cloudStorage, fileFilter });

// router.post('/local', uploadLocal.single('image'), (req, res) => {
//   if (!req.file) {
//     return res.status(400).send({ message: 'No file uploaded' });
//   }

//   res.status(200).send({
//     message: 'Image uploaded successfully',
//     image: `/${req.file.path}`,
//   });
// });

// router.post('/cloud', uploadCloud.single('image'), (req, res) => {
//   if (!req.file) {
//     return res.status(400).send({ message: 'No file uploaded' });
//   }

//   const blob = bucket.file(`${req.file.fieldname}-${Date.now()}${path.extname(req.file.originalname)}`);
//   const blobStream = blob.createWriteStream({
//     metadata: {
//       contentType: req.file.mimetype,
//     },
//   });

//   blobStream.on('error', (err) => {
//     console.log(err);
//     return res.status(400).send({ message: 'Something went wrong' });
//   });

//   blobStream.on('finish', () => {
//     const publicUrl = `https://storage.googleapis.com/${bucket.name}/${blob.name}`;
//     res.status(200).send({
//       message: 'Image uploaded successfully',
//       image: publicUrl,
//     });
//   });

//   blobStream.end(req.file.buffer);
// });

// export default router;


import path from 'path';
import express from 'express';
import multer from 'multer';
import { Storage } from '@google-cloud/storage';

const storage = new Storage({
  projectId: 'capable-code-395103',
  keyFilename: './keys/bucketKey.json',
});

const bucketName = 'capstone-3-toya';
const bucket = storage.bucket(bucketName);

const multerStorage = multer.memoryStorage();

function fileFilter(req, file, cb) {
  const filetypes = /jpe?g|png|webp/;
  const mimetypes = /image\/jpe?g|image\/png|image\/webp/;

  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  const mimetype = mimetypes.test(file.mimetype);

  if (extname && mimetype) {
    cb(null, true);
  } else {
    cb('Error: Images only!', false);
  }
}

const upload = multer({ storage: multerStorage, fileFilter });
const uploadSingleImage = upload.single('image');

const router = express.Router();

router.post('/', (req, res) => {
  uploadSingleImage(req, res, (err) => {
    console.log('Cloud upload endpoint hit');
    if (err) {
      console.log(err);
      return res.status(400).send({ message: err instanceof multer.MulterError ? err.message : 'An error occurred when uploading.' });
    }
    
    if (!req.file) {
      return res.status(400).send({ message: 'No file uploaded' });
    }

    const blob = bucket.file(`${req.file.fieldname}-${Date.now()}${path.extname(req.file.originalname)}`);
    const blobStream = blob.createWriteStream({
      metadata: {
        contentType: req.file.mimetype,
      },
    });

    blobStream.on('error', (err) => {
      console.log(err);
      return res.status(400).send({ message: 'Something went wrong' });
    });

    blobStream.on('finish', () => {
      const publicUrl = `https://storage.googleapis.com/${bucket.name}/${blob.name}`;
      res.status(200).send({
        message: 'Image uploaded successfully',
        image: publicUrl,
      });
    });

    blobStream.end(req.file.buffer);
  });
});

export default router;
