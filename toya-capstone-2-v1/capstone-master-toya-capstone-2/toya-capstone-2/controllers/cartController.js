const Cart = require('../models/Cart');
const Product = require('../models/Product');


module.exports.addToCart = async (req) => {
    try {
        const { userId, productId, quantity } = req.body;

        const product = await Product.findById(productId);
        if (!product) {
            return 'Product not found.';
        }

        if (product.stock <= 0) {
            return 'Product is out of stock!';
        }

        const cart = await Cart.findOneAndUpdate(
            { userId },
            { $push: { items: { productId, quantity, stock: product.stock } } },
            { upsert: true }
        );

        return 'Product added to cart successfully!';
    } catch (error) {
        return 'Adding to cart failed.';
    }
};



module.exports.changeQuantity = async (req) => {
    try {
        const { userId, productId, quantity } = req.body;
        
        const cartItem = await Cart.findOneAndUpdate(
            { userId, 'items.productId': productId },
            { $set: { 'items.$.quantity': quantity } },
            { new: true }
            );
            
            if (!cartItem) {
                return 'Cart item not found.';
            }
            
            const originalQuantity = cartItem.items.find(item => item.productId.toString() === productId).quantity;
            const quantityDifference = originalQuantity - quantity;
            
            if (quantityDifference > 0) {
                const product = await Product.findById(productId);
                
                if (!product) {
                    return 'Product not found.';
                }
                
                product.stock += quantityDifference;
                await product.save();
            }
            
            return 'Product quantity changed successfully!';
    } catch (error) {
        console.error('Error updating item quantity:', error);
        return 'Item update failed.';
    }
};


module.exports.removeFromCart = async (req) => {
    try {
        const { userId, productId } = req.body;
        
        const cart = await Cart.updateOne(
            { userId },
            { $pull: { items: { productId } } }
            );
            
            if (!cart) {
                return 'Cart not found.';
            }
            
            return 'Product removed from cart successfully!';
    } catch (error) {
        return 'Item remove failed.';
    }
};


module.exports.getCartItems = async (userId) => {
    try {
        const cart = await Cart.findOne({ userId }).populate('items.productId');
        
        if (!cart) {
            return 'Cart not found.';
        }
        
        const cartItems = [];
        
        for (const item of cart.items) {
            const product = await Product.findById(item.productId);
            
            if (!product || product.stock <= 0) {
                continue; // Skip invalid or out-of-stock items
            }
            
            const subtotal = product.price * item.quantity;
            
            cartItems.push({
                productId: item.productId._id,
                productName: item.productId.name,
                quantity: item.quantity,
                price: item.productId.price,
                subtotal
            });
        }
        
        const total = cartItems.reduce((acc, item) => acc + item.subtotal, 0);
        
        return { cartItems, total };
    } catch (error) {
        return 'Error in getting cart items.';
    }
};
