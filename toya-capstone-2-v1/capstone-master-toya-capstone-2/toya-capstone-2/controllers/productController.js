const Product = require('../models/Product');

module.exports.createProduct = async (data) => {
    if(data.isAdmin) {
        const newProduct = new Product({
            name: data.product.name,
            description: data.product.description,
            price: data.product.price,
            stock: data.product.stock
        });
        return newProduct.save().then((product, error) => {
            if (error) {
                return 'Error saving product';
            } else {
                return 'Success saving product';
            };
        });
    };
    let message = Promise.resolve("User must be ADMIN to access this page.");
    return message.then((value) => {
        return {value}
    });
};


module.exports.getAllProducts = (isAdmin) => {
    if (!isAdmin) {
        return {error: 'User must be admin to access this page'};
    }
	return Product.find({}).then(result => {
		return result;
	});
};


module.exports.getAllActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
};

module.exports.getProduct = (reqParams) => {
    return Product.findById(reqParams.productId).then(result => {
        return result;
    });
};

module.exports.updateProduct = async (reqParams, reqBody, isAdmin) => {
    if (!isAdmin) {
        throw{error: 'User must be admin to access this page'};
    };
    let updatedProduct = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        stock: reqBody.stock,
        isActive: reqBody.isActive
        
    };
    try {
        const product = await Product.findByIdAndUpdate(reqParams.productId, updatedProduct);
        if (!product) {
            return 'Product not found';
        } else {
            return 'Success updating product';
        };
    } catch (error) {
        return 'Error updating product';
    };
};  

module.exports.archiveProduct = async (reqParams, isAdmin) => {
    if(!isAdmin) {
        return 'User must be admin to archive products';
    };
    let updateActiveField = {
        isActive: false
    };
    try {
        const product = await Product.findByIdAndUpdate(reqParams.productId, updateActiveField);
        if (!product) {
            return 'Product not found';
        } else if (!product.isActive) {
            return 'Product is already archived';
        } else {
            return 'Success archiving the product';
        };
    } catch (error) {
        return 'Error archiving product';
    };
};  

module.exports.activateProduct = async (reqParams, isAdmin) => {
    if(!isAdmin) {
        return 'User must be admin to activate products';
    };
    let updateActiveField = {
        isActive: true
    };
    try {
        const product = await Product.findByIdAndUpdate(reqParams.productId, updateActiveField);
        if (!product) {
            return 'Product not found';
        } else if (product.isActive) {
            return 'Product is already activated'; 
        } else {
            return 'Success activating the product';
        };
    } catch (error) {
        return 'Error activating product';
    };
};  

