const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const Cart = require('../models/Cart');

module.exports.createOrder = async (userId, isAdmin, checkoutFromCart) => {
    if (isAdmin) {
        return 'Only active users are allowed to create orders';
    }
    
    let product;
    
    if (checkoutFromCart) {
        const cart = await Cart.findOne({ userId }).populate('items.productId');
        
        if (!cart) {
            return 'Cart empty';
        }
        
        if (cart.items.length === 0) {
            return 'Cart empty';
        }
        
        const cartItem = cart.items[0]; // Assuming there is only one item in the cart for simplicity
        
        product = cartItem.productId;
        const quantity = cartItem.quantity;
        
        if (quantity <= 0) {
            return 'Invalid quantity';
        }
    } else {
        try {
            product = await Product.findOne({ _id: productId, isActive: true }).exec();
            
            if (!product) {
                console.log(`Product not found. productId: ${productId}`);
                return 'Product not found';
            }
        } catch (error) {
            console.error(`Error retrieving product. Error: ${error}`);
            return 'Error retrieving product';
        }
        
    }
    
    const totalAmount = quantity * product.price;
    
    const order = new Order({
        userId,
        products: [
            {
                productId: product._id,
                quantity,
                totalAmount,
                purchasedOn: Date.now(),
            },
        ],
    });
    
    try {
        product.stock -= quantity;
        await product.save();
        await order.save();
        
        return 'Order created successfully. Thank you!';
    } catch (error) {
        return 'Error creating order';
    }
};



module.exports.getOrderStatus = (userId, orderId) => {
    return Order.findOne({ _id: orderId, userId })
    .populate('products.productId')
    .then((order) => {
        if (!order) {
            return 'Order not found';
        }
        
        let status;
        let statusDate;
        
        if (order.status === 'Cancelled') {
            status = 'Cancelled';
        } else if (order.products.every((product) => product.completedOn)) {
            status = 'Complete';
            statusDate = order.products[0].completedOn;
        } else if (order.products.some((product) => product.deliveredOn)) {
            status = 'Out for delivery';
            statusDate = order.products.find((product) => product.deliveredOn).deliveredOn;
        } else {
            status = 'Pending';
        };
        
        return {
            status,
            statusDate
        };
    })
    .catch((error) => {
        console.error('Error retrieving order:', error);
        return 'Error retrieving order';
    });
};



module.exports.changeOrderStatus = async (orderId, newStatus, completedOn, isAdmin) => {
    if (!isAdmin) {
        return false;
    }
    try {
        const order = await Order.findById(orderId).populate('products.productId');

        if (!order) {
            return 'Order not found';
        };

        if (newStatus === 'Cancelled') {
            await Promise.all(order.products.map(async (product) => {
                const { productId, quantity } = product;
                await Product.findByIdAndUpdate(productId, { $inc: { stock: quantity } });
            }));

            order.status = newStatus;
            await order.save();

            return 'Order status updated successfully and product stock returned';
        };

        if (newStatus === 'Complete') {
            const product = order.products.find((product) => !product.completedOn);

            if (product) {
                product.completedOn = new Date();
            };
        };

        if (newStatus === 'Out for delivery') {
            const product = order.products.find((product) => !product.deliveredOn);

            if (product) {
                product.deliveredOn = new Date();
            };
        };

        order.status = newStatus;
        await order.save();

        return 'Order status updated successfully';
    } catch (error) {
        console.error('Error updating order status:', error);
        return 'Error updating order status';
    };
};

module.exports.getUserOrders = (userId, isUser) => {
    if (!isUser) {
        return Promise.reject('Authenticated user not logged in');
    }
    
    return Order.find({ userId }).then(result => {
        return result;
    });
};


module.exports.getAllOrders = (isAdmin) => {
    if (!isAdmin) {
        return {error: 'Insufficient permission'};
    }
	return Order.find().then(result => {
		return result;
	});
};

