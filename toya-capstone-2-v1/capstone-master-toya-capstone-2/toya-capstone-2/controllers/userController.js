const User = require("../models/User");

const bcrypt = require("bcrypt");

const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
    return User.find({email: reqBody.email}).then(result => {
        if(result.length > 0) {
            return true;
        } else {
            return false;
        };
    });
};

module.exports.registerUser = (reqBody) => {
    let newUser = new User ({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNumber: reqBody.mobileNumber,
        // bcrypt.hashSync for password hashing
        // min no. of salt rounds is 4 and max is 31
        // the recommended is 10-12
        password: bcrypt.hashSync(reqBody.password, 10)
    })
    return newUser.save().then((user, err) => {
        if(err) {
            return false;
        } else {
            return true;
        };
    });
};


module.exports.getProfile = (data) => {
    return User.findById(data.userId).then(result => {
        result.password = "";
        return result;
    });
};



let loginAttempts = {}; 
const resetTimeout = 60 * 60 * 1000; 

module.exports.loginUser = async (reqBody) => {
    const email = reqBody.email;
    const maxAttempts = 3;
    
    if (!loginAttempts[email]) {
        loginAttempts[email] = 1;
    } else {
        loginAttempts[email]++;
    }
    
    if (loginAttempts[email] > maxAttempts) {
        return 'Exceeded maximum incorrect login!';
    }
    
    const result = await User.findOne({ email });
    
    if (result === null) {
        return 'User is not yet registered';
    } else {
        const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
        if (isPasswordCorrect) {
            delete loginAttempts[email];
            return { access: auth.createAccessToken(result) };
        } else {
            return 'Incorrect password!';
        }
    }
};


module.exports.setAdmin = async (reqParams, isAdmin) => {
    if (!isAdmin) {
        return 'User must be admin to promote other users';
    };
    let updateActiveField = {
        isAdmin: true,
        isUser: false
    };
    try {
        const user = await User.findByIdAndUpdate(reqParams.userId, updateActiveField);
        if (!user) {
            return 'No user found';
        };
        if (user.isAdmin) {
            return 'User is already an admin';
        };
        return 'Successfully promoted user to admin';
    } catch (error) {
        return 'Error promoting user';
    };
};

module.exports.getAllUsers = (isAdmin) => {
    if (!isAdmin) {
        return {error: 'User must be admin to access this page'};
    }
	return User.find({}).then(result => {
		return result;
	});
};
