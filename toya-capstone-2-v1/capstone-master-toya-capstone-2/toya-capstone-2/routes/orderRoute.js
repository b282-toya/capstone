const express = require('express');
const router = express.Router();

const orderController = require('../controllers/orderController');
const auth = require('../auth');

router.post('/checkout', auth.verifyUser, (req, res) => {
    const isAdmin = auth.decode(req.headers.authorization).isAdmin;
    const userId = req.user.id;
    const { products, checkoutFromCart } = req.body;

    if (!products || !Array.isArray(products) || products.length === 0) {
        return res.status(400).send('Invalid products data');
    }

    const { productId, quantity } = products[0];

    console.log('Received values:', userId, productId, quantity, checkoutFromCart);

    orderController.createOrder(userId, productId, quantity, isAdmin, checkoutFromCart).then(resultFromController => res.send(resultFromController));
});

router.get('/status/:orderId', auth.verifyUser, (req, res) => {
    const userId = req.user.id;
    const orderId = req.params.orderId;
    
    orderController.getOrderStatus(userId, orderId).then(resultFromController => res.send(resultFromController));
});

router.patch('/status/:orderId', auth.verifyUser, (req, res) => {
    const isAdmin = auth.decode(req.headers.authorization).isAdmin;
    const orderId = req.params.orderId;
    const newStatus = req.body.status;
    
    orderController.changeOrderStatus(orderId, newStatus, isAdmin).then(resultFromController => res.send(resultFromController));
});

router.get('/all-orders', auth.verifyUser, (req, res) => {
    const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    orderController.getAllOrders(isAdmin).then(resultFromController => res.send(resultFromController));
});

module.exports = router;