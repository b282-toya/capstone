const express = require('express');
const router = express.Router();

const userController = require('../controllers/userController');
const orderController = require('../controllers/orderController');
const auth = require('../auth');

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

router.post('/register', (req, res, next) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

router.get("/details", auth.verifyUser, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});

// router.get('/profile/:userId', auth.verifyUser, (req, res) => {
// 	const requestingUserId = auth.decode(req.headers.authorization).id;
// 	const requestedUserId = req.params.userId;
	
// 	if (requestingUserId === requestedUserId || req.user.isAdmin) {
// 		userController.getProfile(requestedUserId)
// 		.then(resultFromController => {
// 			if (resultFromController) {
// 				res.send(resultFromController);
// 			} else {
// 				res.send("User profile not found");
// 			};
// 		});	
// 	};
// });


router.get('/orders', auth.verifyUser, (req, res) => {
	const userId = req.user.id;
	const isUser = req.user.isUser;
	
	orderController.getUserOrders(userId, isUser)
	.then(resultFromController => res.send(resultFromController))
	.catch(error => {
		console.error('Error retrieving user orders:', error);
		res.status(500).send('Error retrieving user orders');
	});
});


router.patch('/set-admin/:userId', auth.verifyUser, (req, res) => {
    const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    userController.setAdmin(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
});


router.get('/all', auth.verifyUser, (req, res) => {
    const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    userController.getAllUsers(isAdmin).then(resultFromController => res.send(resultFromController));
});













module.exports = router;