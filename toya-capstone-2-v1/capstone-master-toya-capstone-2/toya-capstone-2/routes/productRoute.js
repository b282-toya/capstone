const express = require('express');
const router = express.Router();

const productController = require('../controllers/productController');
const auth = require('../auth');



router.post('/new', auth.verifyUser, (req, res) => {
    const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    productController.createProduct(data).then(resultFromController => res.send(resultFromController));
});

router.get('/all', auth.verifyUser, (req, res) => {
    const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    productController.getAllProducts(isAdmin).then(resultFromController => res.send(resultFromController));
});

router.get('/all/active', (req, res) => {
    productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController));
});

router.get('/:productId', auth.verifyUser, (req, res) => {
    productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

router.put('/:productId/update', auth.verifyUser, (req, res) => {
    const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    productController.updateProduct(req.params, req.body, isAdmin).then(resultFromController => res.send(resultFromController));
});

router.patch('/:productId/archive', auth.verifyUser, (req, res) => {
    const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    productController.archiveProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
});

router.patch('/:productId/activate', auth.verifyUser, (req, res) => {
    const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    productController.activateProduct(req.params, isAdmin).then(resultFromController => res.send(resultFromController));
});

module.exports = router;