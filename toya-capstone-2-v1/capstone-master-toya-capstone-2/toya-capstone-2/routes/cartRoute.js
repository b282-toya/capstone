const express = require('express');
const router = express.Router();

const cartController = require('../controllers/cartController');
const auth = require('../auth');


router.post('/add-to-cart', (req, res) => {
    cartController.addToCart(req).then(resultFromController => res.send(resultFromController))
});

router.put('/change-quantity', (req, res) => {
    cartController.changeQuantity(req).then(resultFromController => res.send(resultFromController))
});

router.delete('/remove-from-cart', (req, res) => {
    cartController.removeFromCart(req).then(resultFromController => res.send(resultFromController))
});

router.get('/cart-items/:userId', (req, res) => {
    const userId = req.params.userId;
    cartController.getCartItems(userId).then(resultFromController => res.send(resultFromController));
});

module.exports = router;