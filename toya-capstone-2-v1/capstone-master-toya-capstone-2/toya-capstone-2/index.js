const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
const orderRoute = require("./routes/orderRoute");
const cartRoute = require("./routes/cartRoute");

const app = express();

mongoose.connect("mongodb+srv://toshitoyajr:0WpQikvs8E6C5HSa@wdc028-course-booking.cmjynyq.mongodb.net/capstone2-e-comAPI",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);


mongoose.connection.once("open", () => console.log("We're connected to the cloud database!"));

// Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());


app.use('/users', userRoute);
app.use('/products', productRoute);
app.use('/orders', orderRoute);
app.use('/cart', cartRoute);


app.listen(process.env.PORT || 4000, () => console.log(`Now listening to port ${process.env.PORT || 4000}!`));


// render
// https://capstone-2-toya.onrender.com