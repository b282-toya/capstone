const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: [true, "User ID is required"],
  },
  products: [
    {
      productId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Product",
        required: [true, "Product ID is required"]
      },
      quantity: {
        type: Number,
        required: [true, "Product quantity is required"],
        min: [1, "Product quantity must be at least 1"]
      },
      totalAmount: {
        type: Number,
        required: true
      },
      purchasedOn: {
        type: Date,
        default: Date.now
      },
      deliveredOn: {
        type: Date,
      },
      completedOn: {
        type: Date,
      }
    }
  ],
  status: {
    type: String,
    enum: ["Pending", "Cancelled", "Out for delivery", "Complete"],
    default: "Pending"
  }
});

module.exports = mongoose.model("Order", orderSchema);
