const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Product name is required"]
  },
  description: {
    type: String,
    required: [true, "Product description is required"]
  },
  price: {
    type: Number,
    required: [true, "Product price is required"],
    min: [0, "Product price cannot be less than 0"]
  },
  isActive: {
    type: Boolean,
    default: true
  },
  stock: {
    type: Number,
    required: [true, "Product stock is required"],
    default: [0, "Product stock cannot be less than 0"]
  },
  createdOn: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model("Product", productSchema);
