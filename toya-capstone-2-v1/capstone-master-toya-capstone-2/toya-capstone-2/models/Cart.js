const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: [true, 'User ID is required']
  },
  items: [
    {
      productId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product',
        required: [true, 'Product ID is required']
      },
      quantity: {
        type: Number,
        required: [true, 'Product quantity is required'],
        min: [1, 'Product quantity must be at least 1']
      },
      price: {
        type: Number,
        required: [true, 'Product price is required']
      },
      total: {
        type: Number,
        required: [true, 'Product total is required']
      }
    }
  ]
});


module.exports = mongoose.model('Cart', cartSchema);