import React from 'react';
import { Link } from 'react-router-dom';
import { Row, Col, Button, Container } from 'react-bootstrap';


function AdminDashboard() {
  return (
    <Container>
      <h1>Admin Dashboard</h1>
      <Row className="justify-content-md-center">
        <Col xs={12} md={4}>
          <Link to="/admin/userlist">
            <Button variant="primary" className="dashboard-button" block>
              User List
            </Button>
          </Link>
        </Col>
        <Col xs={12} md={4}>
          <Link to="/admin/productlist">
            <Button variant="primary" className="dashboard-button" block>
              Product List
            </Button>
          </Link>
        </Col>
        <Col xs={12} md={4}>
          <Link to="/admin/orderlist">
            <Button variant="primary" className="dashboard-button" block>
              Order List
            </Button>
          </Link>
        </Col>
      </Row>
    </Container>
  );
}

export default AdminDashboard;