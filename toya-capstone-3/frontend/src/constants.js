export const PRODUCTS_URL = `${process.env.REACT_APP_API_URL}/api/products`;
export const USERS_URL = `${process.env.REACT_APP_API_URL}/api/users`;
export const ORDERS_URL = `${process.env.REACT_APP_API_URL}/api/orders`;
export const PAYPAL_URL = `${process.env.REACT_APP_API_URL}/api/config/paypal`;
export const UPLOAD_URL = `${process.env.REACT_APP_API_URL}/api/upload`;