import styled from "styled-components";

const Container = styled.div`
  height: 30px;
  background-color: orange;
  color: blue;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 14px;
  font-weight: 700;
  padding: 10px;

  @media (max-width: 690px){
    font-size: 10px;
    text-align: center;
  }

  @media (max-width: 420px){
    font-size: 9px;
    text-align: center;
  }
`;

const Announcement = () => {
  return <Container>PROMO: Free Shipping from 1:00am to 1:01am! Hurry! Banzaiii!!!</Container>;
};

export default Announcement;
