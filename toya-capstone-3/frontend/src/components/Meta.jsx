import { Helmet } from 'react-helmet-async';

const Meta = ({ title, description, keywords }) => {
  return (
    <Helmet>
      <title>{title}</title>
      <meta name='description' content={description} />
      <meta name='keyword' content={keywords} />
    </Helmet>
  );
};

Meta.defaultProps = {
  title: 'Ohayo, Banzai!',
  description: 'Ecommerce capstone 3 project',
  keywords: 'cheap, cheaper, cake',
};

export default Meta;
