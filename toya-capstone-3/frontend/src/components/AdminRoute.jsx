import { Navigate, Outlet } from 'react-router-dom';
import { useSelector } from 'react-redux';

const AdminRoute = () => {
  const { userInfo } = useSelector((state) => state.auth);

  console.log('AdminRoute triggered. User info:', userInfo);

  if (!userInfo) {
    // User is not authenticated, redirect to login page
    return <Navigate to="/login" />;
  } else if (!userInfo.isAdmin) {
    // User is authenticated but not an admin, redirect to homepage
    return <Navigate to="/" />;
  } else {
    // User is authenticated and an admin, render the component
    return <Outlet />;
  }
};

export default AdminRoute;
